/*
 * main.c
 *
 * Created: 25.09.2018 19:07:35
 * Author : Egil
 */

/*
 * Include files
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <stdbool.h>
#include <util/atomic.h>
#include "libavr_dotstar_spi/dotstar.h"

#define PONG_LENGTH				143
#define PONG_END_OF_LINE		(PONG_LENGTH/2)
#define PONG_ENDZONE_SIZE		(12)
#define PONG_INITIAL_SPEED		(650)
#define PONG_MAX_SPEED			(150)

#define PONG_MODE_INCREMENTAL	0
#define PONG_MODE_ENDGAME		1	

#define PONG_PIXEL_BRIGHTNESS	0x1f
#define PONG_START_BRIGHTNESS	0x02
#define PONG_END_BRIGHTNESS		0x20
#define OFF						0

#define NO_WINNER_OFF			0
#define NO_WINNER_TIE			1
#define NORMAL_WINNER			2
#define INVERSE_WINNER			3

#define TUGOFWAR_INITIAL_SPEED (5000)


#define PLAYER1PORT				PORTC
#define PLAYER1PINCTRL			PIN0CTRL
#define PLAYER1_bm				PIN0_bm
#define PLAYER1_vect			PORTC_PORT_vect

#define PLAYER2PORT				PORTB
#define PLAYER2PINCTRL			PIN0CTRL
#define PLAYER2_bm				PIN0_bm
#define PLAYER2_vect			PORTB_PORT_vect

#define MODESWITCHPORT			PORTC
#define MODESWITCHPINCTRL		PIN3CTRL
#define MODESWITCH_bm			PIN3_bm

#define GAMESWITCHPORT			PORTC
#define GAMESWITCHPINCTRL		PIN2CTRL
#define GAMESWITCH_bm			PIN2_bm

#define GAME_PONG				0
#define GAME_TUGOFWAR			1

/*
 * Global variables
 */
color_t endzone1[PONG_ENDZONE_SIZE] = {0};
color_t endzone2[PONG_ENDZONE_SIZE] = {0};
color_t pong_color = {.array[0] = 0xff};

int8_t pong = 0;
uint8_t play = 0;
volatile uint16_t button;
uint8_t* buttonarray = (uint8_t*) &button;
uint8_t button_limit = PONG_ENDZONE_SIZE;
uint8_t game = GAME_PONG;


bool debug = false;

uint8_t button_count = 0;
uint8_t timeout = 0;

/*
 * functions
 */
void dotstar_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size);
void decrease_endzone(void);
void reset_game(uint8_t declare_winner);

/*
 * main()
 */
int main(void)
{
	// Variables to keep track of things
	int8_t  pong_direction = 1;
	uint8_t pong_uint = PONG_END_OF_LINE;
	uint8_t pong_mode = PONG_MODE_ENDGAME;

	// Configure clock
#if (F_CPU == 20000000ul)
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
#elif (F_CPU == 10000000ul)
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
#endif

	// Init buttons, interrupt
	PLAYER1PORT.PLAYER1PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	PLAYER2PORT.PLAYER2PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	PLAYER1PORT.INTFLAGS = 0xff;
	PLAYER2PORT.INTFLAGS = 0xff;
	
	// Init toggle switches, polling
	MODESWITCHPORT.MODESWITCHPINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm;
	GAMESWITCHPORT.GAMESWITCHPINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm;

	// Init LEDs to black
	dotstar_init(4);
	dotstar_configure_constant(pong_color, PONG_LENGTH);
	for (uint16_t i = 0; i < PONG_ENDZONE_SIZE; i++)
	{
		endzone1[i].array[0] = 0xff;
		endzone2[i].array[0] = 0xff;
	}
	pong_color.b = 0x20;
	endzone1[0].g = 0x2;
	endzone2[PONG_ENDZONE_SIZE-1].g = 0x2;

	// Configure RTC to wake up device after sleep
	RTC.INTCTRL = RTC_OVF_bm;
	RTC.PER = PONG_INITIAL_SPEED;
	RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;
	RTC.CTRLA = RTC_RTCEN_bm | RTC_PRESCALER_DIV1_gc | RTC_RUNSTDBY_bm;
	set_sleep_mode(SLEEP_MODE_STANDBY);
	sei();
	sleep_mode();

	while(1) {
		if (play)
		{
			if (play == 1)
			{
				// What other games or game modes can be played?
				if (GAMESWITCHPORT.IN & GAMESWITCH_bm)
				{
					game = GAME_PONG;
					set_sleep_mode(SLEEP_MODE_STANDBY);

					if (MODESWITCHPORT.IN & MODESWITCH_bm) {
						pong_mode = PONG_MODE_ENDGAME;
					} else {
						pong_mode = PONG_MODE_INCREMENTAL;
					}

					// Randomize start direction
					while ((PLAYER1PORT.IN & PLAYER1_bm) || (PLAYER2PORT.IN & PLAYER2_bm)) {
						pong_direction = -pong_direction;
					}

					timeout = 0; // Is this timeout reset needed?

				} 
				else 
				{
					game = GAME_TUGOFWAR;
					set_sleep_mode(SLEEP_MODE_IDLE);
					
					while (RTC.STATUS);
					RTC.PER = TUGOFWAR_INITIAL_SPEED;					
					
					timeout = 40;
				}

				// Reset the first end endzone LED
				endzone1[0].g = PONG_START_BRIGHTNESS;
				endzone1[0].r = OFF;
				endzone1[0].b = OFF;
				endzone2[PONG_ENDZONE_SIZE-1].g = PONG_START_BRIGHTNESS;
				endzone2[PONG_ENDZONE_SIZE-1].r = OFF;
				endzone2[PONG_ENDZONE_SIZE-1].b = OFF;
			}
			
			if (play < PONG_ENDZONE_SIZE)
			{
				// reset one endzone LED per rtc tick
				endzone1[play].g = PONG_START_BRIGHTNESS;
				endzone1[play].r = OFF;
				endzone1[play].b = OFF;
				endzone2[PONG_ENDZONE_SIZE-1 - play].g = PONG_START_BRIGHTNESS;
				endzone2[PONG_ENDZONE_SIZE-1 - play].r = OFF;
				endzone2[PONG_ENDZONE_SIZE-1 - play].b = OFF;

				play++;
				button = 0;
			}			
			else if (game == GAME_PONG)
			{
				if (pong_uint < PONG_ENDZONE_SIZE) {
					endzone1[pong_uint].channel = OFF;
					if (pong_uint < button_limit)
					{
						endzone1[pong_uint].g = PONG_START_BRIGHTNESS;
					}
				}
				else if (pong_uint >= PONG_LENGTH-PONG_ENDZONE_SIZE) {
					endzone2[(pong_uint+PONG_ENDZONE_SIZE)-PONG_LENGTH].channel = OFF;
					if (pong_uint >= PONG_LENGTH-button_limit)
					{
						endzone2[(pong_uint+PONG_ENDZONE_SIZE)-PONG_LENGTH].g = PONG_START_BRIGHTNESS;
					}					
				}
				
				// Change direction
				if (button)
				{
					if ((pong < -PONG_END_OF_LINE+button_limit) || (pong > PONG_END_OF_LINE-button_limit))
					{
						if (!timeout) // Debounce
						{
							pong_direction = -pong_direction;
							button_count++;
							timeout = PONG_ENDZONE_SIZE;
						
							uint8_t distance_from_end = PONG_END_OF_LINE - abs(pong);
							if (pong_mode == PONG_MODE_INCREMENTAL) // Increase speed for each change
							{
								if (RTC.PER > PONG_MAX_SPEED)
								{
									while (RTC.STATUS);
									RTC.PER -= RTC.PER/(26-distance_from_end);
								} else if ((button_limit > 3) && (!(button_count % 3))) // Decrease the goal zone
								{
									decrease_endzone();
								}
							} else if (pong_mode == PONG_MODE_ENDGAME) // Speed is controlled by how far from death you are when you push the button
							{
								while (RTC.STATUS);
								RTC.PER = PONG_MAX_SPEED + (distance_from_end * 50);
							
								if ((button_limit > 2) && (!(button_count % 12))) // Decrease the goal zone
								{
									decrease_endzone();
								}
							}
						}			
					}

					button = 0;
				} 

				// Check if out of the board
				pong += pong_direction;
				if(abs(pong) > PONG_END_OF_LINE) {
					reset_game(NORMAL_WINNER);
				}

				if (debug) // "Freeze" time
				{
					while ((PLAYER1PORT.IN & PLAYER1_bm) || (PLAYER2PORT.IN & PLAYER2_bm))
						;
				}
			
				// Update LED array
				pong_uint = pong+PONG_END_OF_LINE;
				if (pong_uint < PONG_ENDZONE_SIZE) {
					endzone1[pong_uint] = rotate_pixel_hue(20, PONG_PIXEL_BRIGHTNESS);
				} else if (pong_uint >= PONG_LENGTH-PONG_ENDZONE_SIZE) {
					endzone2[(pong_uint+PONG_ENDZONE_SIZE)-PONG_LENGTH] = rotate_pixel_hue(20, PONG_PIXEL_BRIGHTNESS);
				} else {
					pong_color = rotate_pixel_hue(20, PONG_PIXEL_BRIGHTNESS);
				}
			} // end GAME_PONG 
			else if (game == GAME_TUGOFWAR) {
				cli();
				int8_t temp_button = buttonarray[1] - buttonarray[0];
				button = 0;
				sei();

				if (pong_uint < PONG_ENDZONE_SIZE) {
					endzone1[pong_uint].channel = OFF;
				}
				else if (pong_uint >= PONG_LENGTH-PONG_ENDZONE_SIZE) {
					endzone2[(pong_uint+PONG_ENDZONE_SIZE)-PONG_LENGTH].channel = OFF;
				}

				if (temp_button != 0) {
					pong += temp_button;
					timeout = 160;
				} 
			
				if (!timeout) {
					reset_game(NO_WINNER_TIE);
				}
			
				if (abs(pong) > PONG_END_OF_LINE) {
					reset_game(INVERSE_WINNER);
				}

				// Update LED array
				pong_uint = pong + PONG_END_OF_LINE;
				if (pong_uint < PONG_ENDZONE_SIZE) {
					endzone1[pong_uint] = rotate_pixel_hue(20, PONG_PIXEL_BRIGHTNESS);
				} else if (pong_uint >= PONG_LENGTH-PONG_ENDZONE_SIZE) {
					endzone2[(pong_uint+PONG_ENDZONE_SIZE)-PONG_LENGTH] = rotate_pixel_hue(20, PONG_PIXEL_BRIGHTNESS);
				} else {
					pong_color = rotate_pixel_hue(20, PONG_PIXEL_BRIGHTNESS);
				}				
			} // end GAME_TUGOFWAR
		} // end play
		else {
			//Blink the middle?
			if (!timeout)
			{
				if (pong_color.channel)
				{
					pong_color.channel = OFF;
				} else {
					pong_color = rotate_pixel_hue(200, PONG_PIXEL_BRIGHTNESS/2);
				}
				timeout = 20;
			}
		}
		
		// Update LEDs
		dotstar_configure_pong(endzone1, endzone2, PONG_ENDZONE_SIZE, pong_color, pong_uint, PONG_LENGTH);
		
		// Go to sleep, wake up on ISR
		sleep_mode();
		
		if (timeout)
		{
			timeout--;
		}
	}
}

void dotstar_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size) {
	color_t off = {.array[0] = 0xff};
	
	dotstar_write_start();
	dotstar_write_array(endzone1, endsize); // first 10 LEDs from array
	
	// middle LEDs either all black or with the pong somewhere
	if ((pong_position < endsize) || (pong_position >= total_size-endsize))
	{
		dotstar_write_constant(off, total_size-(endsize*2));
		} else {
		dotstar_write_constant(off, pong_position-endsize);
		dotstar_write_single(pong_color);
		dotstar_write_constant(off, total_size-endsize-pong_position-1);
	}
	
	dotstar_write_array(endzone2, endsize); // last 10 LEDs from array
	dotstar_write_end(total_size);
}

void decrease_endzone(void)
{
	button_limit--;
	for (uint8_t i = 0; i < PONG_ENDZONE_SIZE; i++)
	{
		if (i < button_limit)
		{
			endzone1[i].g += 5;
			endzone2[PONG_ENDZONE_SIZE-1 - i].g += 5;
		} else {
			endzone1[i].g = 0;
			endzone2[PONG_ENDZONE_SIZE-1 - i].g = 0;
		}

	}
}

ISR(RTC_CNT_vect)   //Just for wakeup
{
	RTC.INTFLAGS = RTC_OVF_bm;
}

ISR(PLAYER1_vect) {
	if (play)
	{
		buttonarray[0]++;
	} else {
		play++;
	}
	
	PLAYER1PORT.INTFLAGS = PLAYER1_bm;
}

ISR(PLAYER2_vect) {
	if (play)
	{
		buttonarray[1]++;
	} else {
		play++;
	}
	
	PLAYER2PORT.INTFLAGS = PLAYER2_bm;
}

void reset_game(uint8_t declare_winner) {
	// set the loosing end colors
	if (declare_winner == INVERSE_WINNER) {
		pong = -pong;
	}
	for (uint8_t i = 0; i < PONG_ENDZONE_SIZE; i++)
	{
		endzone1[i].channel = OFF;
		endzone2[PONG_ENDZONE_SIZE-1 - i].channel = OFF;
		if (declare_winner == NO_WINNER_TIE) { 	// all green endzones
			endzone1[i].g = PONG_END_BRIGHTNESS;
			endzone2[PONG_ENDZONE_SIZE-1 - i].g = PONG_END_BRIGHTNESS;
		} else if (pong < 0) { 				// start endzone red, other green
			endzone1[i].r = PONG_END_BRIGHTNESS;
			endzone2[PONG_ENDZONE_SIZE-1 - i].g = PONG_END_BRIGHTNESS;
		} else if (pong > 0) {							// start endzone green, other red
			endzone1[i].g = PONG_END_BRIGHTNESS;
			endzone2[PONG_ENDZONE_SIZE-1 - i].r = PONG_END_BRIGHTNESS;
		}
	}

	// reset
	play = false;
	button_count = 0;
	pong = 0;
	button_limit = PONG_ENDZONE_SIZE;
	
	while (RTC.STATUS);
	RTC.PER = PONG_INITIAL_SPEED;
	
	timeout = 16; //Start blinking with LED on
	
	//Set another timeout here to avoid starting next game immediately?
}