/*
 * dotstar.h
 *
 * Created: 01.09.2018 00:41:28
 * Author: Egil
 */


#ifndef DOTSTAR_H_
#define DOTSTAR_H_

#define LED_SPI				SPI0
#define LED_DATA_PORT       PORTA
#define LED_DATA_PIN        PIN1_bm
#define LED_SCK_PORT        PORTA
#define LED_SCK_PIN         PIN3_bm

/*
 * The structs you need
 */

typedef __uint24 uint24_t;

typedef union {
	uint8_t array[4];
	struct {
		uint8_t brightness : 5;
		uint8_t reserved : 3;
		union {
			struct {
				uint8_t b;
				uint8_t g;
				uint8_t r;
				};
				uint24_t channel;
			};
	};
} color_t;

typedef struct {
	uint16_t h;
	uint8_t s;
	uint8_t b;
} hsb_t;

/*
 * Defines that might be interesting to use
 */

#define GREEN   (0x800000)
#define RED     (0x008000)
#define BLUE    (0x000080)
#define YELLOW  (0x808000)
#define MAGENTA (0x008080)
#define CYAN    (0x800080)
#define WHITE   (0x808008)
#define BLACK   (0x000000)
#define ORANGE  (0x408000)
#define INDIGO  (0x004080)
#define VIOLET  (0x00486A)

/*
 * Private functions
 */

//static void LED_Send_Bit(uint8_t Data);

/*
 * Public functions
 */

void dotstar_init(void);
void dotstar_configure_single_rgb(color_t Color);
void dotstar_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length);
void dotstar_configure_constant_rgb(color_t Color, uint16_t LED_string_length);
void dotstar_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size);
color_t hsb2rgb(hsb_t led_array);

#endif /* DOTSTAR_H_ */

